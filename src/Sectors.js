import React from 'react'
import axios from 'axios';
import './Sectors.css';

class Sectors extends React.Component {
    constructor(props) {
        super(props);
        this.state = { sectors: {}, name: "", category: "", isAgree: false, disabled: true};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }

    componentDidMount() {
        this.loadSectorsDataFromServer();
    }

    loadSectorsDataFromServer() {
        axios.get(`http://localhost:8080/api/category`)
            .then(response => {
                const sectors = response.data;
                console.dir(response)
                this.setState( {sectors} );
            })
            .catch(error => {
                console.log(error.response);
            });
    }

    handleCheckboxChange(event) {
        this.setState({
            isAgree: !this.state.isAgree
        });
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        const newChoice = {
            name: this.state.name,
            category: this.state.category,
            isAgree: this.state.isAgree,
        };
        axios.post(`http://localhost:8080/api/choice`, {newChoice});
            this.setState( {newChoice} );
            window.location.reload();
    }

    render() {
        const { sectors } = this.state;

        return (
            <form className="sectorsForm" onSubmit={this.handleSubmit}>
                <div> Please enter your name and pick the Sectors you are currently involved in. </div>
                <div> Name:
                    <input type="text" name="name" placeholder="Type your name here" onChange={this.handleChange}/>
                </div>

                <div>
                    <div> Sectors:

                    </div>
                </div>

                <div>
                    <input type="checkbox"
                           name="isAgree"
                           checked={this.state.isAgree}
                           onChange={this.handleCheckboxChange}/>
                    Agree to terms
                </div>
                <button type="save" className="btn btn-primary"
                        onClick={this.handleSubmit}>Save</button>
            </form>
        );
    }
}

export { Sectors }