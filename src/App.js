import React from 'react';
import './App.css';
import {Sectors} from './Sectors'

function App() {
  return (
      <div className="App">
        <Sectors />
      </div>
  );
}

export default App;
